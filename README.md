# Dokumentacja projektu (draft)

### Autorzy

Michał Czyż (+funkcje)

- backend
- frontend

Wojciech Ładyga

- backend
- frontend
- scrum master

### Technologie

- frontend -> react
- backend -> node.js
- baza danych -> relacyjna baza danych (Postgres)

### a. Identyfikacja zagadnienia biznesowego

Współcześnie większość handlu detalicznego odbywa się poprzez Internet. Lecz są branże, której sprzedaż nadal w większości dominuje w formie klasycznej. Są to między innymi sklepy odzieżowe. Jednak trend powoli zaczyna się odwracać i powstaje oraz więcej sklepów sprzedających ubrania poprzez sieć.
Wychodząc naprzeciw wymaganiom rynku nasz zespół zaprojektuje i wdroży sklep internetowy z odzieżą oparty o najnowsze i aktualne trendy technologiczne.
Sklep oferować będzie produkty dla każdej osoby, której celem będzie zakupienie nowej części garderoby. Nie stawia żadnych wymagań dotyczących płci i wieku, więc każdy znajdzie coś dla siebie od dynamicznego 13-latka po 100-latka.
Nowoczesny i prosty design pozwoli korzystać ze sklepu nawet osobą z podstawowymi umiejętnościami komputerowymi. Równocześnie stawia podstawowe wymagania jakimi jest dostęp użytkownika do urządzeń elektronicznych podpiętych do Internetu.

### b. Wymagania systemowe i funkcjonalne

Współczesny sklep Internetowy musi zawiera szereg funkcjonalności aby móc przyciągnąć do siebie klienta. Funkcjonalności zawarte w projekcie to:

- System autoryzacji użytkownika
- Wybór wielu sposobów płatności
- Wybór wielu sposobów dostawy towaru
- Formularz pozwalający o ubieganie się zwrotu pieniędzy za produkt
- Sekcja z ocenami produktu
- Sekcja z komentarzami na temat produktu
- Koszyk z produktami, pozwalający na późniejsze zawarcie transakcji
- Potwierdzenie zawarcia transakcji emailem
- System sortowania produktów pod względem
  - Ceny
  - Opinie
- System filtrowania produktów
  - Rodzaj odzieży (buty, koszulki, itp.)
  - Pod względem płci
- System wystawiania oceny produktowi
- System wystawiania komentarza pod produktem

Wymagania systemie:  
Aplikacja ta jest aplikacją internetową opartą o architekturę REST. Zbudowana jest z 2 modułów. Modułu backendowego opartego o node.js i modułu frontendowego wykorzystującego bibliotekę react. Komunikacja między modułami wykorzystuje protokół http. Dane przesyłane są w formacie JSON i korzystają z metod oferowanych przez http w celu wykonywania odpowiednich akcji po obu stronach aplikacji.
Dodatkowo aby zasilić aplikację danymi i przechowywać dane użytkownika została wykorzystana baza danych Postgres. Jest to relacyjna baza danych zapewniająca jednolitość i bezpieczeństwo danych klientów.
