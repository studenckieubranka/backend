const { PrismaClient } = require("@prisma/client");
const users = require("./data/users");
const cloths = require("./data/cloths");
const brands = require("./data/brands");
const cloth_pieces = require("./data/cloth_pices");
const prisma = new PrismaClient();

async function main() {
  for (const user of users) {
    let created = await prisma.user.create({
      data: user,
    });
  }

  await prisma.brand.createMany({
    data: brands,
  });

  for (const cloth of cloths) {
    let created = await prisma.clothType.create({
      data: cloth,
    });
  }

  let products = await prisma.cloth.findMany();
  for (const product of products) {
    await prisma.cloth.update({
      where: {
        id: product.id,
      },
      data: {
        clothPieces: {
          create: cloth_pieces,
        },
      },
    });
  }
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
