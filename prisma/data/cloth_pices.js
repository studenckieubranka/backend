const cloth_pices = [
  {
    available: 10,
    size: "XS",
  },
  {
    available: 10,
    size: "S",
  },
  {
    available: 10,
    size: "M",
  },
  {
    available: 10,
    size: "L",
  },
  {
    available: 10,
    size: "XL",
  },
];

module.exports = cloth_pices;
