const users = [
  // password: Qazwsx123!
  {
    email: "admin@admin.com",
    password: "$2b$10$Z6Ko7HfvchJEtaV8mRqT0e9Fi7q00isRvknuE4Q0P.nLxg0p0dV/q",
    role: "ADMIN",
    userDetails: {
      create: {
        name: "Jan",
        surname: "Kowalski",
        phone: "111222333",
        address: {
          create: {
            province: "Małopolska",
            city: "Kraków",
            street: "Łojasiewicza",
            house_no: "11",
            postal: "30-348",
          },
        },
      },
    },
  },
  // password: PoCoMItoH@sło123
  {
    email: "k.nowak@email.com",
    password: "$2b$10$X9uQcGuBisNsQ10iz/4hqeLgGLUGzekNsG89/of6/SkttyBheDAUW",
    role: "CUSTOMER",
    userDetails: {
      create: {
        name: "Karolina",
        surname: "Nowak",
        phone: "444555666",
        address: {
          create: {
            province: "Mazowsze",
            city: "Warszawa",
            street: "Radziejowicka",
            house_no: "26",
            postal: "01-475",
          },
        },
      },
    },
  },
  // password: JP70%wujek_w_policji!
  {
    email: "seba.polska@email.com",
    password: "$2b$10$xG1oHoOfo86WX8YNnQRBlu3fFzF8mS70PQRS6hfCSRN8Ha3KIqgnW",
    role: "CUSTOMER",
    userDetails: {
      create: {
        name: "Sebastian",
        surname: "Polak",
        phone: "997100203",
        address: {
          create: {
            province: "Śląsk",
            city: "Sosnowiec",
            street: "Żelazna",
            house_no: "13",
            postal: "41-208",
          },
        },
      },
    },
  },
];

module.exports = users;
