const cloths = [
  {
    typeName: "T-SHIRT",
    cloths: {
      create: [
        {
          // https://www.lacoste.pl/lacoste-t-shirt-meski/th0146/46b/
          name: "T-SHIRT MĘSKI",
          price: 249.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["Bs_9AkQy_1CD9LVuLt-Es.jpg", "5DYfLFW268Rec-4ZSMaBE.jpg", "4mXJwdwq4J0nlxL5MFMbH.jpg", "IA_SyOKXbmUrHRc9qkJsn.jpg"]
        },
        {
          // https://www.lacoste.pl/lacoste-t-shirt-meski/th0120/20l/
          name: "T-SHIRT MĘSKI",
          price: 299.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["PdS2Bb_57mynpXE8TSpu1.jpg", "YC2xn-MyOsgJAqyKP7wHd.jpg", "OZ-QcBGcukz2zAoJSn_of.jpg", "3_C4d86XhE-PDHDVfEYUL.jpg"]
        },
        {
          // https://www.lacoste.pl/lacoste-damski-t-shirt-z-dekoltem-w-ksztalcie-litery-v/tf0999/166/
          name: "DAMSKI T-SHIRT Z DEKOLTEM W KSZTAŁCIE LITERY V",
          price: 219.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "FEMALE",
          imagesUrls: ["U6bw4WIzy42uRTl9ZNv0t.jpg", "ALp6vmGejyZmexDO4xchv.jpg", "GcJ3dtO50tGHcyPqpwPKB.jpg", "glD3x09ceHnWNXTdJyvYQ.jpg"]
        },
        {
          // https://www.lacoste.pl/tf1243/fpq/
          name: "T-SHIRT DAMSKI",
          price: 299.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "FEMALE",
          imagesUrls: ["YQzhpk93l9QRLfGPsdgY_.jpg", "-OoKQPKnuymXM4d7jhWeb.jpg", "CC-6bH_RKvCJ9AaSz1a99.jpg", "i4pd5nZYPBSuoCW9E9Uo3.jpg"]
        },
        {
          // https://www.lacoste.pl/lacoste-t-shirt-damski-z-okraglym-wycieciem-pod-szyja/tf0823/23p/
          name: "T-SHIRT DAMSKI Z OKRĄGŁYM WYCIĘCIEM POD SZYJĄ",
          price: 299.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "FEMALE",
          imagesUrls: ["Qan55B3Ix5ZaDnCqseRqi.jpg", "AuxZr_MHiayU2ucYllsw2.jpg", "d7xkjQd-GxV10lFm_-Ni2.jpg", "Df6qtbuIgfGA2rOPP3KPD.jpg"]
        },
        // https://www.housebrand.com/pl/pl/1364h-60x/koszulka-m-hl21-tsm515
        {
          name: "Koszulka z nadrukiem Mind Over Matter",
          price: 39.99,
          brand: {
            connect: {
              name: "HOUSE",
            },
          },
          for: "MALE",
          imagesUrls: ["3cf6koLNbLc8TS0fAkWcx.jpg", "QSlW5k1B6ajRgDrwMejaC.jpg", "FHnjs1HZPZ4lRGnR5Xfj-.jpg"]
        },
      ],
    },
  },
  {
    typeName: "KURTKI",
    cloths: {
      // https://www.lacoste.pl/lacoste-meska-lekka-przeciwdeszczowa-wiatrowka-na-zamek-blyskawiczny/bh3201/132/
      create: [
        {
          name: "MĘSKA LEKKA PRZECIWDESZCZOWA WIATRÓWKA",
          price: 779.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["260IxH9nfB57qcdOq0ZZ0.jpg", "qneTcOMRjvNScg1nYizz5.jpg", "2Fo6-JjjW6xLEGB4_gytE.jpg", "pHnEcCUlC1-gdJajvoMVH.jpg"],
        },
        // https://www.lacoste.pl/bh2164/64l/
        {
          name: "KURTKA MĘSKA",
          price: 999.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["9Le90Qk4VOYCn_pb_3-J_.jpg", "1o-BcDg_aupR5rPW5pcWK.jpg", "_PCmoMyI7-KexlmvTOyEQ.jpg", "6xaSgHL4w_EI07HgLQmJJ.jpg"],
        },
        // https://www.lacoste.pl/lacoste-plaszcz-damski/bf1924/24g-3/
        {
          name: "PŁASZCZ DAMSKI",
          price: 3499.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "FEMALE",
          imagesUrls: ["_BMj0j4OsoFzDyFOq6MAJ.jpg", "an_EGXyMhShmaQ-fZa4Q7.jpg", "CUtObTT1vy8j2Aw69s3G8.jpg", "M2P-aF59RUgZ820ECa8pS.jpg"],
        },
      ]
    }
  },
  {
    typeName: "SPODNIE",
    cloths: {
      create: [
        {
          // https://www.lacoste.pl/lacoste-luzne-spodnie-meskie/hh0160/60g/
          name: "LUŹNE SPODNIE MĘSKIE",
          price: 579.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["jSdj6bjImy_RjgHbRyFvb.jpg", "SdpM5ppuqAo1GAFxcHWK-rEw1UwreoclHIW7JGPDD0.jpg", "VhGGETLuoc876XZYZp3RF.jpg", "nSNp-_XXjqSmqg-dh3kyb.jpg"]
        },
        {
          // https://www.lacoste.pl/lacoste-meskie-bawelniane-jednolite-spodnie-regular-fit-typu-chino/hh0094/hde/
          name: "MĘSKIE BAWEŁNIANE JEDNOLITE SPODNIE REGULAR FIT TYPU CHINO",
          price: 549.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["8sk8ERQEiIdbEZoJF9ue3.jpg", "KT1iwOq9wI8pO5AwKwGS0.jpg", "wur9CeRfNNivA4DO_i3rx.jpg", "rEw1UwreoclHIW7JGPDD0.jpg"]
        },
        {
          // https://www.lacoste.pl/hh7510/hyy/
          name: "SPODNIE MĘSKIE",
          price: 499.99,
          brand: {
            connect: {
              name: "LACOSTE",
            },
          },
          for: "MALE",
          imagesUrls: ["6tDjlltMdkIhqcguqiupk.jpg", "DPOWlHdHGfwED18791aSA.jpg", "g6HI_cTIOw632_u9PClIs.jpg", "X5YnotEJSH9e358AsYJhS.jpg"]
        },
        // https://www.reserved.com/pl/pl/3138c-09m/spodnie-m-re
        {
          name: "Bawełniane spodnie chino",
          price: 149.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "MALE",
          imagesUrls: ["82AOrGOAEZjdEbh6y3MDP.jpg", "zYbKE6Qezx4EitZVMCyU4.jpg", "Djymk-pY524qieLGCEn1e.jpg", "pT7L5jUeZb4zypeAjtEDm.webp"]
        },

        // https://www.reserved.com/pl/pl/yv116-90x/spodnie-m-re
        {
          name: "Chinosy slim ze strukturalnej tkaniny",
          price: 129.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "MALE",
          imagesUrls: ["WdQtiVasEScRsNBtAaOrS.webp", "oP63HKLH7XmhEoc-Q1wRn.webp", "y9qj10JAErfMMG8P2Fzz6.webp", "vXtk7AdJoIM1miZR8tYXp.jpg"]
        },

        // https://www.reserved.com/pl/pl/yv600-59x/spodnie-m-re
        {
          name: "Bawełniane spodnie slim fit",
          price: 89.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "MALE",
          imagesUrls: ["CgkmVdVVkUeiaeKwcZvki.webp", "WSbvTGSIFxrmPI3QuQyhq.webp", "0yLsNzN6hSanRfREh2cNg.webp", "15TF8hfByEzvfkhWziphz.webp"]
        },
        // https://www.reserved.com/pl/pl/1256c-08x/spodnie-k-re
        {
          name: "Spodnie z plisowanej dzianiny",
          price: 119.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "FEMALE",
          imagesUrls: ["RfEStB3FKxpmCPZINLEqy.jpg", "zFLrbFc8Am9snHonqOlVS.jpg", "zLzol4kNhlriRbvy0Th7p.jpg"]
        },
        // https://www.reserved.com/pl/pl/9710b-01x/spodnie-k-re
        {
          name: "Cygaretki z kantem",
          price: 139.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "FEMALE",
          imagesUrls: ["uTfpPiCZqKpOa2ihXpzoQ.jpg", "PL2LBnOjpICR3NSw5ckJb.jpg", "K04xn0rCCJSbbecOy8UWx.jpg", "hcBlY85xAbXrwoAtpj-il.jpg"]
        },
        // https://www.reserved.com/pl/pl/0081c-80x/spodnie-k-re
        {
          name: "Spodnie z szerokimi nogawkami",
          price: 139.99,
          brand: {
            connect: {
              name: "RESERVED",
            },
          },
          for: "FEMALE",
          imagesUrls: ["NktKte2mGB0VLRHHSZQHM.jpg", "y0iXQuOYqaKgjF0oQr8tQ.jpg", "jVOEz2Wppv5R7k0Qz10WV.jpg", "dnVeGQxOF1z-sc7FHCwiJ.jpg"]
        },
        // https://www.pullandbear.com/pl/dzwony-w-zeberke-l08677302
        {
          name: "Dzwony w zeberkę",
          price: 99.9,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "FEMALE",
          imagesUrls: ["H8bbGjBEBhTYAWVqwBd7a.webp", "OMkjg-d8Y0Fr5exuIZinz.webp", "VdmICnV6l2cS-w5a_Iqtu.webp", "LcZQW_S6xs8-Hy__CHfs9.webp"]
        },
        // https://www.pullandbear.com/pl/spodnie-jogger-basic-z-piki-l04676519
        {
          name: "Spodnie jogger basic z piki",
          price: 79.9,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "MALE",
          imagesUrls: ["MnZPZOaUufkM1xobKFmVJ.webp", "BFrJpAaH8pTaLcG35dJep.webp", "4fkguT8jh1dbK1gesI9wK.webp", "OcGDT84m5Wm9lRY2XskaP.webp"]
        },
        // https://www.pullandbear.com/pl/dopasowane-spodnie-skinny-fit-l09678532
        {
          name: "Dopasowane spodnie skinny fit",
          price: 119.0,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "MALE",
          imagesUrls: ["Axtcs0XhtqUnB0lFjk1R3.webp", "MgeMuifQSN9aupgF8r-T3.webp", "54e081Bx9ptsPDl-6X4Gm.webp", "DojA2IZHRnhBSz5dSv_in.webp"]
        },
        // https://www.pullandbear.com/pl/kolorowe-lniane-spodnie-jogger-l04676543
        {
          name: "Jednokolorowe lniane spodnie jogger",
          price: 119.0,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "MALE",
          imagesUrls: ["v0sOstZBxrGvbbRYdrY2Q.webp", "Hsot-Zo3dbD314AGTS9kg.webp", "Hp7HpveKBR5XbTMnvqQzl.webp", "YKg5mdnUQzweK7LfYI-ue.webp"]
        },
        // https://www.pullandbear.com/pl/spodnie-jogger-z-mieszanki-lnu-l04676544
        {
          name: "Spodnie jogger z mieszanki lnu",
          price: 119.0,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "MALE",
          imagesUrls: ["13PhfEIg9-cz2v9I5XmoL.webp", "Yh5cctK4uYixLyuu8v9Iu.webp", "wG-HJ3JvhFbt_ZXVLGRQo.webp", "Fb4sJJDUUu-GzT8a4P1-W.webp"]
        },
        // https://www.pullandbear.com/pl/spodnie-cargo-z-tkaniny-ripstop-l04676530
        {
          name: "Spodnie cargo z tkaniny ripstop",
          price: 119.0,
          brand: {
            connect: {
              name: "PULL&BEAR",
            },
          },
          for: "MALE",
          imagesUrls: ["wk4D83K_D6kbFhEFIM1x6.webp", "wZWvFzlzaFJjTiJBwG6Jy.webp", "BVxz1XqhFIrTCNXb2sVZH.webp", "0N403WW1_YB1gG73NNk7j.jpg"]
        },
        // https://www.sinsay.com/pl/pl/2118b-99x/spodnie-k-si
        {
          name: "Spodnie paperbag",
          price: 39.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "FEMALE",
          imagesUrls: ["Dkgin0qPK_HUVVhYlEj7E.webp", "ZvVRgGZpE_RUSRyFOGRJf.webp", "GfsRuBF4Ir__1CL0O1WCr.webp", "2b96M4jEod5fmhFoZHeoK.webp"]
        },
        // https://www.sinsay.com/pl/pl/9386d-09m/spodnie-k-si
        {
          name: "Spodnie dressowe",
          price: 49.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "FEMALE",
          imagesUrls: ["e92gz77t4unKqFO_0FdUy.webp", "JgNqWShCypnRtmVmouwfR.webp", "gHTovQEokN2dMRoMbVPpa.webp", "mxJXyZFO_ENSI2jbEgrHO.webp"]
        },
        // https://www.sinsay.com/pl/pl/2113b-99x/spodnie-k-si
        {
          name: "Spodnie chino z zapięciem",
          price: 59.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "FEMALE",
          imagesUrls: ["u58zi-ixhK-nW1x_8a0Zk.webp", "jxO873-GSBQTtikgutow-.webp", "iY5RZSNzORBKQxccWXpWD.webp", "ShkwY-XuLk8RiH_1XMtXn.webp"]
        },
        // https://www.sinsay.com/pl/pl/2088b-12x/spodnie-k-si
        {
          name: "Woskowane spodnie",
          price: 69.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "FEMALE",
          imagesUrls: ["l20hOKd-VQrDk_3iDVwM0.webp", "ORsX-eFBcnCARae3D-yZC.webp", "sr6H7CnbyD5VYASNHELb1.webp", "dIP740nOtMJmsyfn1mxJJ.webp"]
        },
        // https://www.sinsay.com/pl/pl/2018b-03p/spodnie-k-si
        {
          name: "Eleganckie spodnie ECO AWARE",
          price: 49.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "FEMALE",
          imagesUrls: ["-cr4f65U0VWARTfzDpTLK.webp", "JkKQSfBYgYyPQmQXUdZRI.webp", "sXSwI-6ZzQt5NDQuoBeIr.webp", "IJYH3M0CjjV9xsXJFonxs.webp"]
        },
        // https://www.sinsay.com/pl/pl/0260a-mlc/spodnie-m-si
        {
          name: "Spodnie jogger camo",
          price: 79.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["g6oPrVmyht4BEiPSav7xa.webp", "2zVtYtR7PyBe453nNJ9cp.webp", "KEB7UHxlyib6vw_iWq-8a.webp", "e7TyEMyAXpz5_HDRwKUJY.webp"]
        },
        // https://www.sinsay.com/pl/pl/0268a-mlc/spodnie-m-si
        {
          name: "Eleganckie spodnie slim fit w kratę",
          price: 79.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["ZbMjKUO2EtwIaZNHjhfT7.webp", "o-6EPUTLEmf_7nQMT90bc.webp", "TIGxMO1t3Kglipw0vcEf6.jpg", "TXBKMjkTH3RqR8N3ujEmC.webp"]
        },
        // https://www.sinsay.com/pl/pl/0264a-99x/0264a-99x-t08-spodnie-m-si
        {
          name: "Spodnie typu cargo",
          price: 79.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["510VTOp5GtYCz5H6I99WX.webp", "HXqE4BbKBCincJiiUhYtG.jpg", "k5Kcz-iFDmLs83HwQi4Ld.webp", "WSzgdfeBBuNiwZyyuU_tv.webp"]
        },
        {
          // https://www.sinsay.com/pl/pl/0257a-09m/spodnie-m-si
          name: "Joggery z nadrukiem",
          price: 49.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["xvd6dBkBE1vXmqyBeSF-0.jpg", "eIKQbwB7H5A2KvFSHZ5YC.jpg", "6MykpFfBDTBdGtbd9zmR3.jpg", "T3I6W0PsZIjk7fDiPcZGV.jpg"]
        },
        // https://www.sinsay.com/pl/pl/0226a-99x/0226a-99x-t09-spodnie-m-si?algolia_query_id=e8dafde52f0e4d217b760a8ba430877c
        {
          name: "Cienkie joggery ECO AWARE",
          price: 79.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["lSb5kPiLYsOeClkCWaGyP.jpg", "A21AvPkFjJ6noaeZZF5Uk.jpg", "BqXTd5tZ7tRYr__k6cSHO.jpg"]
        },
        // https://www.sinsay.com/pl/pl/0240a-82x/0240a-82x-t05-spodnie-m-si
        {
          name: "Spodnie sztruksowe ECO AWARE",
          price: 79.99,
          brand: {
            connect: {
              name: "SINSAY",
            },
          },
          for: "MALE",
          imagesUrls: ["uIqF5VeoNpp5NaWvJbVS5.jpg", "3mFhhZhhrs8JOPSFuqElS.jpg", "Rv0wHx5V8aslvU6LHBfyM.jpg", "5sA7DHaOp0YQclVerOTrs.jpg"]
        },
      ],
    },
  },
];

module.exports = cloths;
