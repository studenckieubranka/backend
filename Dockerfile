FROM node:15

WORKDIR /usr/src/app

RUN git clone https://gitlab.com/studenckieubranka/backend.git

WORKDIR /usr/src/app/backend

RUN git checkout -t origin/feature/prepare-docker

RUN npm install

EXPOSE 8081 5555

CMD ["npx", "dotenv", "-e", ".env.production", "npm", "run", "fullstart"]