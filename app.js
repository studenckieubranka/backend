var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");

var indexRouter = require("./routes/index");
const userRouter = require("./routes/user/user");
const productRouter = require("./routes/products/product");
const categoryRouter = require("./routes/categories/category");
const brandRouter = require("./routes/brands/brand");
const orderRouter = require("./routes/order/order");
const imagesRouter = require('./routes/images/images');

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
// app.use(express.static(path.join(__dirname, 'public')));

app.use("/", indexRouter);
app.use("/api/v1/user", userRouter);
app.use("/api/v1/product", productRouter);
app.use("/api/v1/category", categoryRouter);
app.use("/api/v1/brand", brandRouter);
app.use("/api/v1/order", orderRouter);
app.use("/api/v1/images", imagesRouter);

module.exports = app;
