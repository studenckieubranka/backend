const bcrypt = require("bcrypt");
console.log(require("crypto").randomBytes(64).toString("hex"));

bcrypt.genSalt(10, (err, salt) => {
  bcrypt.hash("JP70%wujek_w_policji!", salt, (err, res) => {
    console.log(res);
  });
});
var res = bcrypt.compareSync(
  "Qazwsx123!",
  "$2b$10$IX30FOA2Ap/GEPT4Q3oOmuoitpIyasWnFnczwmg5ZtpLKw7Z/uimW"
);
console.log(res);
