const prisma = require("../client");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const bcrypt = require("bcrypt");

const JWT_TOKEN_EXPIRATION = "1800s";
dotenv.config();

function hashPassword(password) {
  let salt = bcrypt.genSaltSync(Number(process.env.BCRYPT_SALT_ROUNDS));
  return bcrypt.hashSync(password, salt);
}

class UserService {
  async getByEmail(email) {
    return await prisma.user.findUnique({
      where: {
        email: email,
      },
    });
  }

  async getUserDetailsByEmail(email) {
    return await prisma.user.findUnique({
      where: {
        email: email,
      },
      select: {
        email: true,
        userDetails: {
          select: {
            name: true,
            surname: true,
            phone: true,
            address: true,
          },
        },
      },
    });
  }

  async getUserDetailsByUserId(userId) {
    return await prisma.userDetails.findFirst({
      where: {
        userId: userId,
      },
    });
  }

  generateToken(email) {
    return jwt.sign({ email: email }, process.env.JWT_SECRET, {
      expiresIn: JWT_TOKEN_EXPIRATION,
    });
  }

  async save(user) {
    let password = hashPassword(user.password);
    try {
      await prisma.user.create({
        data: {
          ...user,
          password: password,
          userDetails: {
            create: {
              ...user.userDetails,
              address: {
                create: user.userDetails.address,
              },
            },
          },
        },
      });
    } catch (err) {
      console.log(err);
      return err;
    }

    return null;
  }
}

module.exports = new UserService();
