const prisma = require("../client");

class BrandService {
  async getBrands() {
    return await prisma.brand.findMany();
  }
}

module.exports = new BrandService();
