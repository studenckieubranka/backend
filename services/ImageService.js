const sharp = require('sharp');
const fs = require('fs');
const path = require('path');
const imageDir = path.join(__dirname, '..', 'images')

class ImageService {
    getImage(filename, width, height,) {
        const readStream = fs.createReadStream(path.join(imageDir, filename));
        width = parseInt(width);
        height = parseInt(height);
        let transform = prepareTransform(width, height);
        return readStream.pipe(transform);
    }
}

function prepareTransform(width, height) {
    let transform = sharp();

    if (width || height) {
        transform = transform.resize(width, height);
    }

    return transform;
}

module.exports = new ImageService();