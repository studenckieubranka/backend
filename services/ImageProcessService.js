const fs = require("fs");
const {nanoid} = require('nanoid');

class ImageProcessService {
  processImages(data) {
    let array = [];
    let images = JSON.parse(data);
    console.log("ok parse data");
    images.forEach((photo, i) => {
      let regexp = "data:type;base64,";
      regexp = regexp.replace("type", photo.type);
      let base64Data = JSON.parse(photo.content).data.replace(regexp, "");
      let name = nanoid() + photo.name;
      fs.writeFileSync("./images/" + name, base64Data, "base64");
      array.push(name);
    });
    return array;
  }
}

module.exports = new ImageProcessService();
