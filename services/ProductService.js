const prisma = require("../client");

const pageSize = 8;

class ProductService {
  async getPage(pageNumber, category, sex, sortBy, brands, from, to) {
    let brandsFilter = [];
    if (brands.length !== 0) {
      brands.split(",").forEach((el) => {
        brandsFilter.push({
          name: el,
        });
      });
    }

    let descending = "desc";
    if (sortBy !== "" || sortBy !== null) {
      switch (sortBy) {
        case "nameUp":
          descending = "asc";
          sortBy = "name";
          break;
        case "nameDown":
          sortBy = "name";
          break;
        case "priceUp":
          descending = "asc";
          sortBy = "price";
          break;
        case "priceDown":
          sortBy = "price";
          break;
      }
    }
    let filter = {
      for: sex !== "" ? sex : {},
      type: category !== "" ? { typeName: category } : {},
      brand: brands.length !== 0 ? { OR: brandsFilter } : {},
      AND: [
        from !== "" ? { price: { gte: from } } : {},
        to !== "" ? { price: { lte: to } } : {},
      ],
    };

    let cloths = await prisma.cloth.findMany({
      skip: pageNumber * pageSize,
      take: pageSize,
      include: {
        brand: {
          select: {
            name: true,
          },
        },
      },
      where: filter,
      orderBy: sortBy !== "" ? { [sortBy]: descending } : {},
    });

    let numberOfRecords = await prisma.cloth.count({
      where: filter,
    });
    let count = Math.ceil(numberOfRecords / pageSize);
    return { cloths, count };
  }

  async getProductById(productId) {
    let cloth = await prisma.cloth.findUnique({
      where: {
        id: productId,
      },
      include: {
        clothPieces: {
          where: {
            available: {
              gt: 0,
            },
          },
          select: {
            size: true,
          },
        },
      },
    });
    cloth.clothPieces = cloth.clothPieces.map((clothPiece) => {
      return clothPiece.size;
    });
    return cloth;
  }

  async saveProduct(data, imgArray) {
    if (data.custom_category) {
      await prisma.clothType.create({
        data: {
          typeName: data.category.toUpperCase(),
        },
      });
    }
    if (data.custom_brand) {
      await prisma.brand.create({
        data: {
          name: data.brand,
        },
      });
    }

    let idType = await prisma.clothType.findFirst({
      where: {
        typeName: data.category.toUpperCase(),
      },
    });

    let idProduct = await prisma.cloth.create({
      data: {
        name: data.name,
        price: data.price,
        brand: {
          connect: {
            name: data.brand,
          },
        },
        for: data.for,
        type: {
          connect: {
            id: idType.id,
          },
        },
        imagesUrls: imgArray
      },
    });

    for (const size_ of data.sizes) {
      await prisma.clothPiece.create({
        data: {
          size: size_,
          available: Number(data.quantity),
          cloth: {
            connect: {
              id: idProduct.id,
            },
          },
        },
      });
    }
  }
}

module.exports = new ProductService();
