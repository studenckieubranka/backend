const prisma = require("../client");

class CategoryService {
  async getCategories() {
    return await prisma.clothType.findMany();
  }
}

module.exports = new CategoryService();
