let router = require('express').Router();
const ImageService = require('../../services/ImageService');

router.get('/:imageFilename', (req, res) => {
    const filename = req.params.imageFilename;
    const [, format] = filename.split('.');
    const width = req.query.width;
    const height = req.query.height;
    res.type(`image/${format}`)
    ImageService.getImage(filename, width, height).pipe(res);
});

module.exports = router;