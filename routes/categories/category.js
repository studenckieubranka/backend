let router = require("express").Router();
const CategoryService = require("../../services/CategoryService");

router.get("/all", (req, res) => {
  CategoryService.getCategories().then((categories) => {
    res.status(200).json(categories);
  });
});

module.exports = router;
