let router = require("express").Router();
const BrandService = require("../../services/BrandService");

router.get("/all", (req, res) => {
  BrandService.getBrands().then((brands) => {
    res.status(200).json(brands);
  });
});

module.exports = router;
