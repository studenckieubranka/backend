var express = require("express");
var router = express.Router();
const GeneratePdfService = require("../../services/GeneratePdfService");

router.post("/", (req, res) => {
  GeneratePdfService.generate(req.body).then((r) => {
    res.sendStatus(200);
  });
  res.sendStatus(500);
});

module.exports = router;
