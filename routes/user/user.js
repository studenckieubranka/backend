let router = require("express").Router();
const bcrypt = require("bcrypt");
const UserService = require("../../services/UserService");
const saltRounds = 10;

router.get("/info/:email", async (req, res) => {
  UserService.getUserDetailsByEmail(req.params.email).then((user) => {
    console.log("Give me info");
    if (user) {
      res.status(200).json(user).end();
    } else {
      console.log("error");
    }
  });
});

router.post("/login", async (req, res) => {
  let credentials = req.body;
  UserService.getByEmail(credentials.email).then((user) => {
    UserService.getUserDetailsByUserId(user.id).then((data) => {
      bcrypt.compare(credentials.password, user.password, (err, result) => {
        if (err) {
          console.log(err);
        }
        if (result) {
          const userData = {
            email: credentials.email,
            name: data.name + " " + data.surname,
            authorization: UserService.generateToken(credentials.email),
            role: user.role,
          };
          res.status(200).json(userData).end();
        } else {
          res.status(401).end();
        }
      });
    });
  });
});

router.post("/register", (req, res) => {
  let user = req.body;
  console.log(user);
  UserService.save(user).then((err) => {
    if (err) {
      res
        .status(400)
        .json({ message: "User with specified email already exists." });
    }
    res.status(201).end();
  });
});

module.exports = router;
