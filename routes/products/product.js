let router = require("express").Router();
const ProductService = require("../../services/ProductService");
const ImageProcessService = require("../../services/ImageProcessService");
const fileParser = require("express-multipart-file-parser");
router.use(fileParser);

router.get("/page/:pageNumber", (req, res) => {
  let pageNumber = req.params.pageNumber;

  let category = req.query.category ? req.query.category : "";
  let sex = req.query.sex
    ? req.query.sex === "women"
      ? "FEMALE"
      : "MALE"
    : "";
  let sortBy = req.query.sortBy ? req.query.sortBy : "";
  let brands = req.query.brands ? req.query.brands : [];
  let from = req.query.from ? req.query.from : "";
  let to = req.query.to ? req.query.to : "";

  ProductService.getPage(
    pageNumber,
    category,
    sex,
    sortBy,
    brands,
    from,
    to
  ).then((cloths) => {
    res.status(200).json(cloths);
  });
});

router.get("/:id", (req, res) => {
  let productId = Number(req.params.id);
  ProductService.getProductById(productId).then((cloth) => {
    res.status(200).json(cloth);
  });
});

router.post("/add", (req, res) => {
  let arrayImg = ImageProcessService.processImages(req.body.image);
  let data = JSON.parse(req.body.data);
  console.log(arrayImg)
  ProductService.saveProduct(data, arrayImg).then(() => {
    res.sendStatus(200);
  });
});

module.exports = router;
